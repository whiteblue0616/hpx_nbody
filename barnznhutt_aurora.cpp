#include <fenv.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <random>
#include <omp.h>
#include <chrono>

#include "Bhtree.cpp"

void initializeBodies(struct body *bods);

void interactBodies(struct body *b);

void singleInteraction(struct body *a, struct body *b);

double magnitude(const vec3 &v);

void updateBodies(struct body *b);

void renderBodies(struct body *b);

void runBenchmark(struct body *b, int stepCount);

int main(const int argc, const char **argv) {
    int steps = 10;

    if (argc > 1) {
        steps = atoi(argv[1]);
    }

    std::cout << "Steps: " << steps << std::endl;

    struct body *bodies = new struct body[NUM_BODIES];

    initializeBodies(bodies);

    runBenchmark(bodies, steps);

    return 0;
}

long getTimeMillSecond() {
    std::chrono::steady_clock::duration d = std::chrono::steady_clock::now().time_since_epoch();
    std::chrono::milliseconds mil = std::chrono::duration_cast<std::chrono::milliseconds>(d);
    return mil.count();
}

void initializeBodies(struct body *bods) {
    using std::uniform_real_distribution;

    std::default_random_engine gen(0);
    double angle;
    double radius;
    double velocity;
    struct body *current;

    uniform_real_distribution<double> randAngle(0.0, 200.0 * PI);
    uniform_real_distribution<double> randRadius(INNER_BOUND, SYSTEM_SIZE);
    uniform_real_distribution<double> randHeight(0.0, SYSTEM_THICKNESS);

    //STARS
    velocity = 0.67 * sqrt((G * SOLAR_MASS) / (4 * BINARY_SEPARATION * TO_METERS));
    //STAR 1
    current = &bods[0];
    current->position.x = 0.0;///-BINARY_SEPARATION;
    current->position.y = 0.0;
    current->position.z = 0.0;
    current->velocity.x = 0.0;
    current->velocity.y = 0.0;//velocity;
    current->velocity.z = 0.0;
    current->mass = SOLAR_MASS;
    //STAR 2


    ///STARTS AT NUMBER OF STARS///
    double totalExtraMass = 0.0;
    for (int index = 1; index < NUM_BODIES; index++) {

        angle = randAngle(gen);
        radius = sqrt(SYSTEM_SIZE) * sqrt(randRadius(gen));
        velocity = pow(((G * (SOLAR_MASS + ((radius - INNER_BOUND) / SYSTEM_SIZE) * EXTRA_MASS * SOLAR_MASS))
                        / (radius * TO_METERS)), 0.5);
        current = &bods[index];
        current->position.x = radius * cos(angle);
        current->position.y = radius * sin(angle);
        current->position.z = randHeight(gen) - SYSTEM_THICKNESS / 2;
        current->velocity.x = velocity * sin(angle);
        current->velocity.y = -velocity * cos(angle);
        current->velocity.z = 0.0;

        current->mass = (EXTRA_MASS * SOLAR_MASS) / NUM_BODIES;
        totalExtraMass += (EXTRA_MASS * SOLAR_MASS) / NUM_BODIES;
    }
}


void runBenchmark(struct body *b, int stepCount) {
    std::cout << "start benchmark for step: " << stepCount << std::endl << std::flush;
    long start = getTimeMillSecond();
    for (int i = 0; i < stepCount; i++) {
        interactBodies(b);
        renderBodies(b);
    }
    long end = getTimeMillSecond();
    std::cout << "all  time cost: " << (end - start) << " ms" << std::endl << std::flush;
}


void interactBodies(struct body *bods) {
    // Sun interacts individually
    if (DEBUG_INFO) { std::cout << "\nCalculating Force from star..." << std::flush; }
    struct body *sun = &bods[0];
#pragma omp parallel for
    for (int bIndex = 1; bIndex < NUM_BODIES; bIndex++) {
        struct body *a = sun;
        struct body *b = &bods[bIndex];

        vec3 posDiff;
        posDiff.x = (a->position.x - b->position.x) * TO_METERS;
        posDiff.y = (a->position.y - b->position.y) * TO_METERS;
        posDiff.z = (a->position.z - b->position.z) * TO_METERS;
        const vec3 &v=posDiff;
        double dist = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
        double F = TIME_STEP * (G * a->mass * b->mass) / ((dist * dist + SOFTENING * SOFTENING) * dist);

        a->accel.x -= F * posDiff.x / a->mass;
        a->accel.y -= F * posDiff.y / a->mass;
        a->accel.z -= F * posDiff.z / a->mass;
        b->accel.x += F * posDiff.x / b->mass;
        b->accel.y += F * posDiff.y / b->mass;
        b->accel.z += F * posDiff.z / b->mass;

    }

    // Build tree
    Octant &&proot = Octant(0, /// center x
                            0, /// center y
                            0.1374, /// center z Does this help?
                            60 * SYSTEM_SIZE);
    Bhtree *tree = new Bhtree(std::move(proot));

    for (int bIndex = 1; bIndex < NUM_BODIES; bIndex++) {
        if (tree->octant().contains(bods[bIndex].position)) {
            tree->insert(&bods[bIndex]);
        }
    }

    // loop through interactions
#pragma omp parallel for
    for (int bIndex = 1; bIndex < NUM_BODIES; bIndex++) {
        if (tree->octant().contains(bods[bIndex].position)) {
            tree->interactInTree(&bods[bIndex]);
        }
    }
    // Destroy tree
    delete tree;
    //
    updateBodies(bods);
}

void singleInteraction(struct body *a, struct body *b) {
    vec3 posDiff;
    posDiff.x = (a->position.x - b->position.x) * TO_METERS;
    posDiff.y = (a->position.y - b->position.y) * TO_METERS;
    posDiff.z = (a->position.z - b->position.z) * TO_METERS;

    const vec3 &v=posDiff;
    double dist = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
    double F = TIME_STEP * (G * a->mass * b->mass) / ((dist * dist + SOFTENING * SOFTENING) * dist);

    a->accel.x -= F * posDiff.x / a->mass;
    a->accel.y -= F * posDiff.y / a->mass;
    a->accel.z -= F * posDiff.z / a->mass;
    b->accel.x += F * posDiff.x / b->mass;
    b->accel.y += F * posDiff.y / b->mass;
    b->accel.z += F * posDiff.z / b->mass;
}


void updateBodies(struct body *bods) {
    double mAbove = 0.0;
    double mBelow = 0.0;
#pragma omp for
    for (int bIndex = 0; bIndex < NUM_BODIES; bIndex++) {
        struct body *current = &bods[bIndex];
        current->velocity.x += current->accel.x;
        current->velocity.y += current->accel.y;
        current->velocity.z += current->accel.z;
        current->accel.x = 0.0;
        current->accel.y = 0.0;
        current->accel.z = 0.0;
        current->position.x += TIME_STEP * current->velocity.x / TO_METERS;
        current->position.y += TIME_STEP * current->velocity.y / TO_METERS;
        current->position.z += TIME_STEP * current->velocity.z / TO_METERS;
    }
}


void renderBodies(struct body *b) {
    /// ORTHOGONAL PROJECTION
#pragma omp parallel for
    for (int index = 0; index < NUM_BODIES; index++) {
        struct body *current = &b[index];

        int x = (WIDTH / 2.0) * (1.0 + current->position.x / (SYSTEM_SIZE * RENDER_SCALE));
        int y = (HEIGHT / 2.0) * (1.0 + current->position.y / (SYSTEM_SIZE * RENDER_SCALE));

        if (x > DOT_SIZE && x < WIDTH - DOT_SIZE &&
            y > DOT_SIZE && y < HEIGHT - DOT_SIZE) {
            const vec3 &v = current->velocity;
            double vMag = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
        }
    }
}



