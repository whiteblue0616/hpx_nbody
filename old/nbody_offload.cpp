#include <fenv.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <random>
#include <chrono>

#include <hpx/hpx_init.hpp>
#include <hpx/include/iostreams.hpp>
#include <hpx/include/parallel_algorithm.hpp>
#include <hpx/include/parallel_for_loop.hpp>
#include <hpx/include/lcos.hpp>

#include <boost/iterator/counting_iterator.hpp>

#include <ve_offload.h>

#include "Bhtree.cpp"


void interactBodies(struct body *bods, struct veo_proc_handle *proc, struct veo_thr_ctxt *ctx,
                    uint64_t handle);

void singleInteraction(struct body *a, struct body *b);

double magnitude(const vec3 &v);

void updateBodies(struct body *b);

double toPixelSpace(double p, int size);

void renderBodies(struct body *b);

void runBenchmark(struct body *b, int stepCount);

void future_hpx(struct body *a, struct body *b, hpx::lcos::local::counting_semaphore *semaphore);

void future_aurora(struct body *a, struct body *b, struct veo_proc_handle *proc, struct veo_thr_ctxt *ctx,
                   uint64_t handle, hpx::lcos::local::counting_semaphore *semaphore);


long getTimeMillSecond() {
    std::chrono::steady_clock::duration d = std::chrono::steady_clock::now().time_since_epoch();
    std::chrono::milliseconds mil = std::chrono::duration_cast<std::chrono::milliseconds>(d);
    return mil.count();
}

void initializeBodies(struct body *bods) {

    using std::uniform_real_distribution;
    uniform_real_distribution<double> randAngle(0.0, 200.0 * PI);
    uniform_real_distribution<double> randRadius(INNER_BOUND, SYSTEM_SIZE);
    uniform_real_distribution<double> randHeight(0.0, SYSTEM_THICKNESS);
    std::default_random_engine gen(0);

    //STARS
    double velocity = 0.67 * sqrt((G * SOLAR_MASS) / (4 * BINARY_SEPARATION * TO_METERS));
    //STAR 1
    struct body *start = &bods[0];
    start->position.x = 0.0;///-BINARY_SEPARATION;
    start->position.y = 0.0;
    start->position.z = 0.0;
    start->velocity.x = 0.0;
    start->velocity.y = 0.0;//velocity;
    start->velocity.z = 0.0;
    start->mass = SOLAR_MASS;

    ///STARTS AT NUMBER OF STARS///
    double totalExtraMass = 0.0;

    for (int index = 1; index < NUM_BODIES; index++) {
        double tmpAngle = randAngle(gen);
        double tmpRadius = sqrt(SYSTEM_SIZE) * sqrt(randRadius(gen));
        double tmpVelocity = pow(
                ((G * (SOLAR_MASS + ((tmpRadius - INNER_BOUND) / SYSTEM_SIZE) * EXTRA_MASS * SOLAR_MASS))
                 / (tmpRadius * TO_METERS)), 0.5);

        bods[index].position.x = tmpRadius * cos(tmpAngle);
        bods[index].position.y = tmpRadius * sin(tmpAngle);
        bods[index].position.z = randHeight(gen) - SYSTEM_THICKNESS / 2;
        bods[index].velocity.x = tmpVelocity * sin(tmpAngle);
        bods[index].velocity.y = -tmpVelocity * cos(tmpAngle);
        bods[index].velocity.z = 0.0;
        bods[index].mass = (EXTRA_MASS * SOLAR_MASS) / NUM_BODIES;

        totalExtraMass += (EXTRA_MASS * SOLAR_MASS) / NUM_BODIES;
    }
    hpx::cout << "\nTotal Disk Mass: " << totalExtraMass;
    hpx::cout << "\nEach Particle weight: " << (EXTRA_MASS * SOLAR_MASS) / NUM_BODIES
              << "\n______________________________\n";
}


void runBenchmark(struct body *b, int stepCount, struct veo_proc_handle *proc, struct veo_thr_ctxt *ctx,
                  uint64_t handle) {
    hpx::cout << "start benchmark for step: " << stepCount << hpx::endl << hpx::flush;
    long start = getTimeMillSecond();
    for (int i = 0; i < stepCount; i++) {
        interactBodies(b, proc, ctx, handle);
        renderBodies(b);
    }
    long end = getTimeMillSecond();
    hpx::cout << "all  time cost: " << (end - start) << " ms" << hpx::endl << hpx::flush;
}


void future_hpx(struct body *a, struct body *b, hpx::lcos::local::counting_semaphore *semaphore) {
    singleInteraction(a, b);
    semaphore->signal();
}

void future_aurora(struct body *a, struct body *b, struct veo_proc_handle *proc, struct veo_thr_ctxt *ctx,
                   uint64_t handle, hpx::lcos::local::counting_semaphore *semaphore) {

    uint64_t sym = veo_get_sym(proc, handle, "offload_func");

    struct veo_args *argp = veo_args_alloc();
    veo_args_set_stack(argp, VEO_INTENT_IN, 0, (char *) a, sizeof(a));
    veo_args_set_stack(argp, VEO_INTENT_IN, 1, (char *) b, sizeof(b));

    uint64_t retVal;
    uint64_t id = veo_call_async(ctx, sym, argp);
    int ret = veo_call_wait_result(ctx, id, &retVal);

    hpx::cout << "get result: " << ret << hpx::endl;

    semaphore->signal();
}

void interactBodies(struct body *bods, struct veo_proc_handle *proc, struct veo_thr_ctxt *ctx,
                    uint64_t handle) {
    // Sun interacts individually
    struct body *sun = &bods[0];

    hpx::lcos::local::counting_semaphore semaphore;

    for (int bIndex = 1; bIndex < NUM_BODIES; bIndex++) {
        if (bIndex % 2 == 0) {
            hpx::async(future_hpx, sun, &bods[bIndex], &semaphore);
        } else {
            hpx::async(future_aurora, sun, &bods[bIndex], proc, ctx, handle, &semaphore);
        }
    }


    semaphore.wait(NUM_BODIES - 1);


    // Build tree
    Octant &&proot = Octant(0, /// center x
                            0, /// center y
                            0.1374, /// center z Does this help?
                            60 * SYSTEM_SIZE);
    Bhtree *tree = new Bhtree(std::move(proot));

    for (int bIndex = 1; bIndex < NUM_BODIES; bIndex++) {
        if (tree->octant().contains(bods[bIndex].position)) {
            tree->insert(&bods[bIndex]);
        }
    }

    // loop through interactions
    hpx::parallel::for_loop(hpx::parallel::execution::par, 1, NUM_BODIES, [&](int bIndex) {
        if (tree->octant().contains(bods[bIndex].position)) {
            tree->interactInTree(&bods[bIndex]);
        }
    });

    // Destroy tree
    delete tree;
    //
    updateBodies(bods);
}


void singleInteraction(struct body *a, struct body *b) {
    vec3 posDiff;
    posDiff.x = (a->position.x - b->position.x) * TO_METERS;
    posDiff.y = (a->position.y - b->position.y) * TO_METERS;
    posDiff.z = (a->position.z - b->position.z) * TO_METERS;
    double dist = magnitude(posDiff);
    double F = TIME_STEP * (G * a->mass * b->mass) / ((dist * dist + SOFTENING * SOFTENING) * dist);

    a->accel.x -= F * posDiff.x / a->mass;
    a->accel.y -= F * posDiff.y / a->mass;
    a->accel.z -= F * posDiff.z / a->mass;
    b->accel.x += F * posDiff.x / b->mass;
    b->accel.y += F * posDiff.y / b->mass;
    b->accel.z += F * posDiff.z / b->mass;
}

double magnitude(const vec3 &v) {
return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

void updateBodies(struct body *bods) {

    hpx::parallel::for_loop(hpx::parallel::execution::par, 0, NUM_BODIES, [&](int bIndex) {
        struct body *current = &bods[bIndex];

        current->velocity.x += current->accel.x;
        current->velocity.y += current->accel.y;
        current->velocity.z += current->accel.z;
        current->accel.x = 0.0;
        current->accel.y = 0.0;
        current->accel.z = 0.0;
        current->position.x += TIME_STEP * current->velocity.x / TO_METERS;
        current->position.y += TIME_STEP * current->velocity.y / TO_METERS;
        current->position.z += TIME_STEP * current->velocity.z / TO_METERS;
    });

}


void renderBodies(struct body *b) {
    /// ORTHOGONAL PROJECTION
    hpx::parallel::for_loop(hpx::parallel::execution::par, 0, NUM_BODIES, [&](int index) {
        struct body *current = &b[index];

        int x = toPixelSpace(current->position.x, WIDTH);
        int y = toPixelSpace(current->position.y, HEIGHT);

        if (x > DOT_SIZE && x < WIDTH - DOT_SIZE &&
            y > DOT_SIZE && y < HEIGHT - DOT_SIZE) {
            double vMag = magnitude(current->velocity);
        }
    });

}

double toPixelSpace(double p, int size) {
    return (size / 2.0) * (1.0 + p / (SYSTEM_SIZE * RENDER_SCALE));
}


int hpx_main(int argc, char **argv) {
    int steps = 10;

    if (argc > 1) {
        steps = atoi(argv[1]);
    }

    // create VE proc
    hpx::cout << "Initializing VE.." << hpx::endl;

    struct veo_proc_handle *proc = veo_proc_create(0);
    if (proc == NULL) {
        hpx::cout << "veo_proc_create() failed!" << hpx::endl;
        return -1;
    }

    uint64_t handle = veo_load_library(proc, "./libnbody.so");
    hpx::cout << "handle = " << handle << hpx::endl;

    struct veo_thr_ctxt *ctx = veo_context_open(proc);

    hpx::cout << "Initializing data.." << hpx::endl;


    hpx::cout << "Steps: " << steps << hpx::endl << hpx::flush;

    struct body *bodies = new struct body[NUM_BODIES];

    initializeBodies(bodies);

    runBenchmark(bodies, steps, proc, ctx, handle);

    return hpx::finalize();
}

int main(int argc, char *argv[]) {
    return hpx::init(argc, argv);
}

