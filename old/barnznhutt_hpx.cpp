#include <fenv.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <random>
#include <sys/time.h>
#include <chrono>


#include <hpx/hpx_init.hpp>
#include <hpx/include/iostreams.hpp>
#include <hpx/include/parallel_algorithm.hpp>
#include <hpx/include/parallel_for_loop.hpp>
#include <hpx/include/lcos.hpp>

#include <boost/iterator/counting_iterator.hpp>

#define SOFTENING 1e-9f

typedef struct {
    float x, y, z, vx, vy, vz;
} Body;

void randomizeBodies(float *data, int n) {
    for (int i = 0; i < n; i++) {
        data[i] = 2.0f * (rand() / (float) RAND_MAX) - 1.0f;
    }
}

long getTimeMillSecond() {
    std::chrono::steady_clock::duration d = std::chrono::steady_clock::now().time_since_epoch();
    std::chrono::milliseconds mil = std::chrono::duration_cast<std::chrono::milliseconds>(d);
    return mil.count();
}


void future(Body *p, float dt, int n, int i, int j, hpx::lcos::local::counting_semaphore *semaphore) {

    float dx = p[j].x - p[i].x;
    float dy = p[j].y - p[i].y;
    float dz = p[j].z - p[i].z;
    float distSqr = dx * dx + dy * dy + dz * dz + SOFTENING;
    float invDist = 1.0f / sqrtf(distSqr);
    float invDist3 = invDist * invDist * invDist;

    p[i].vx += dt * dx * invDist3;
    p[i].vy += dt * dy * invDist3;
    p[i].vz += dt * dz * invDist3;
    semaphore->signal();
}

void bodyForce(Body *p, float dt, int n) {

    hpx::lcos::local::counting_semaphore semaphore;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {

            hpx::async(future, p, dt, n, i, j, &semaphore);
        }
    }

    semaphore.wait(n * n);

}


int hpx_main(int argc, char **argv) {

    int nBodies = 30000;
    if (argc > 1) nBodies = atoi(argv[1]);

    const float dt = 0.01f; // time step
    const int nIters = 10;  // simulation iterations

    double totalTime = 0.0;

    for (int iter = 1; iter <= nIters; iter++) {
        long start = getTimeMillSecond();

        int bytes = nBodies * sizeof(Body);
        float *buf = (float *) malloc(bytes);
        Body *p = (Body *) buf;

        randomizeBodies(buf, 6 * nBodies); // Init pos / vel data

        bodyForce(p, dt, nBodies); // compute interbody forces

        for (int i = 0; i < nBodies; i++) { // integrate position
            p[i].x += p[i].vx * dt;
            p[i].y += p[i].vy * dt;
            p[i].z += p[i].vz * dt;
        }
        long end = getTimeMillSecond();

        if (iter > 1) { // First iter is warm up
            totalTime += (end - start);
        }

        free(buf);
    }
    double avgTime = totalTime / (double) (nIters - 1);

    std::cout << "average time: " << avgTime << std::endl;

    printf("%d Bodies: average %0.3f Billion Interactions / second\n", nBodies, 1e-9 * nBodies * nBodies / avgTime);
    return hpx::finalize();
}

int main(int argc, char *argv[]) {
    return hpx::init(argc, argv);
}





